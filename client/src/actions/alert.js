import uuidv4 from 'uuid/v4';
import { SET_ALERT, REMOVE_ALERT } from './types';

export const setAlert = (msg, alert) => dispatch => {
  const id = uuidv4();
  dispatch({
    type: SET_ALERT,
    payload: { msg, alert, id }
  });

  setTimeout(() => dispatch({ type: REMOVE_ALERT, payload: id }), 5000);
};

export function closeAlert(id) {
  return {
    type: REMOVE_ALERT,
    payload: id
  };
}
// ↓の書き方もOK
// export const closeAlert = id => dispatch => {
//   dispatch({
//     type: REMOVE_ALERT,
//     payload: id
//   });
// };
