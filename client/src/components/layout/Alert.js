import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { closeAlert } from '../../actions/alert';
import CloseButton from '../parts/CloseButton';

const mapDispatchToProps = dispatch => ({
  closeAlert(id) {
    dispatch(closeAlert(id));
  }
});

// export const Alert = ({ alerts }) =>
export class Alert extends React.PureComponent {
  static propTypes = {
    alerts: PropTypes.array.isRequired,
    closeAlert: PropTypes.func.isRequired,
    onClick: PropTypes.func
  };
  constructor() {
    super();
    this.onClose = this.onClose.bind(this);
  }

  onClose(e, id) {
    e.preventDefault();
    const { closeAlert } = this.props;
    closeAlert(id);
  }

  render() {
    const { alerts } = this.props;
    return (
      alerts !== null &&
      alerts.length > 0 &&
      alerts.map(alert => (
        <div key={alert.id} className={`alert alert-${alert.alertType}`}>
          {alert.msg}
          <CloseButton clickHandler={e => this.onClose(e, alert.id)} />
        </div>
      ))
    );
  }
}

// Alert.propTypes = {
//     alerts: PropTypes.array.isRequired
// };

const mapStateToProps = state => ({
  alerts: state.alert
});

export default connect(mapStateToProps, mapDispatchToProps)(Alert);
