import React from 'react';

const DeleteButton = ({ onDeleteHandler, content, account }) => (
  <button className='btn btn-danger' onClick={onDeleteHandler}>
    {account && <i className='fas fa-user' />}
    {content}
  </button>
);
export default DeleteButton;
