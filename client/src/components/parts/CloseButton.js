import React from 'react';

const CloseButton = ({ clickHandler }) => (
  <a href='!#' className='close-btn' onClick={clickHandler}>
    x
  </a>
);
export default CloseButton;
