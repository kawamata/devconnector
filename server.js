const express = require('express');
const connectDB = require('./config/db');

const app = express();

// Connect Database
connectDB();

// セッション設定
const cookieParser = require('cookie-parser');
const session = require('express-session');
const redis = require('redis');
const RedisStore = require('connect-redis')(session);
const redisClient = redis.createClient();
app.use(cookieParser());
app.use(
  session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false,
    store: new RedisStore({ client: redisClient }),
    cookie: {
      httpOnly: true,
      secure: false,
      maxage: 1000 * 60 * 60
    }
  })
);

// Init middleware
app.use(express.json({ extended: false }));

app.get('/', (req, res) => {
  res.json({
    sessionID: req.sessionID
  });
  res.send('API Running...');
});

app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/profile', require('./routes/api/profile'));
app.use('/api/posts', require('./routes/api/posts'));

// 開発環境では5000を使う：process.env.PORTが設定されてなければ5000
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
